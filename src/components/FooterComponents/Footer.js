import React from 'react';

const Footer = () => {
    return(
        <footer className="footer">
            <div className="container clearfix">
                <p className="footer_info">
                    email: <a className="footer_email-link" href="#">info@domain.com</a>
                </p>
                <nav className="nav">
                    <ul className="nav__list">
                        <li className="nav__item">
                            <a className="nav__link nav__link--themedark">about us</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link nav__link--themedark nav__link--active">work</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link nav__link--small">services</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link nav__link--themedark">blog</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link nav__link--themedark">careers</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link nav__link--themedark">contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer>

    )
}
export default Footer;