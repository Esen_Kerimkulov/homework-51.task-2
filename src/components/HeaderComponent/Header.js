import React from 'react';

const Header = (props) => {
    return (
        <header className="header">
            <div className="container clearfix">
                <a href="#" className="logo header__logo">
                    <b className="logo__bold">. one</b>Town
                </a>
                <nav className="nav header__nav">
                    <ul className="nav__list">
                        <li className="nav__item">
                            <a className="nav__link">about us</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link--active">work</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link">services</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link">blog</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link">careers</a>
                        </li>
                        <li className="nav__item">
                            <a className="nav__link">contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
    )
};

export default Header;