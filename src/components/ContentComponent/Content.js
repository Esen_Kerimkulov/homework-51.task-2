import React from 'react';
import work1 from '../../img/work-1.jpg';
import work2 from '../../img/work-2.jpg';
import work3 from '../../img/work-3.jpg';
import work4 from '../../img/work-4.jpg';
import work5 from '../../img/work-5.jpg';
import work6 from '../../img/work-6.jpg';
import work7 from '../../img/work-7.jpg';
import work8 from '../../img/work-8.jpg';
import work9 from '../../img/work-9.jpg';

const Content = (props) => {
    return (
        <div className="works">
            <div className="container">
                <h1 className="title-main">our works</h1>
                <div className="works__items">
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work1} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">App UI Kit Pro</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work2} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">vintage poster effects</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work3} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">ecommerce line icons</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work4} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">iOS7 Icon Concepts</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work5} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">BlueBox: Flat PSD Template</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work6} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">iPhone App website</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work7} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">Leather UI Elements PSD</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work8} alt=""/>
                        </a>
                        <figcaption className="work-item__descr">Coffee Cups PSD</figcaption>
                    </figure>
                    <figure className="work-item">
                        <a href="#" className="work-item__link">
                            <img className="work-item__img" src={work9} alt="_"/>
                        </a>
                        <figcaption className="work-item__descr">14 Badges in 4 Styles</figcaption>
                    </figure>
                </div>
            </div>
        </div>
    )
};
                    export default Content;

