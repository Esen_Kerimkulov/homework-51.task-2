import React, { Component } from 'react';
import Header from './components/HeaderComponent/Header';
import Content from './components/ContentComponent/Content';
import Footer from './components/FooterComponents/Footer';
import './App.css';
import work from './img/work-1.jpg';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Content/>
        <Footer/>

      </div>
    );
  }
}

export default App;
